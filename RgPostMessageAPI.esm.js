!function (e, n) {
    "object" == typeof exports && "undefined" != typeof module ? module.exports = n() : "function" == typeof define && define.amd ? define(n) : e.RgPostMessageAPI = n()
}(this, function () {
    "use strict";
    var e = {
        PAUSE_REQUESTED: "rg_pause",
        RESUME: "rg_resume",
        PAUSE: "rg_pauseComplete",
        WARN: "rg_pauseWarn",
        UPDATE_BALANCE: "rg_updateBalance",
        SET_SOUNDS: "rg_setSounds",
        GET_SOUND_STATE: "rg_getSounds"
    }, n = {NOT_PAUSED: 0, PAUSE_PENDING: 1, PAUSE_REPORT_PENDING: 2, PAUSED: 3}, t = n.NOT_PAUSED, a = void 0;

    function r(n) {
        o(e.WARN, {message: n})
    }

    function o(e, n) {
        var t = new CustomEvent(e);
        t.data = n, window.dispatchEvent(t)
    }

    !function () {
        if ("function" == typeof window.CustomEvent) return !1;

        function e(e, n) {
            n = n || {bubbles: !1, cancelable: !1, detail: null};
            var t = document.createEvent("CustomEvent");
            return t.initCustomEvent(e, n.bubbles, n.cancelable, n.detail), t
        }

        e.prototype = window.Event.prototype, window.CustomEvent = e
    }();
    var i = {
        Event: e, requestPause: function (i) {
            i && i.autoPlayOnly || (t === n.PAUSED ? r("Already paused") : t === n.PAUSE_PENDING && r("Pause already requested, waiting for the current round to end"), t = n.PAUSE_PENDING), a = i, o(e.PAUSE_REQUESTED, i)
        }, paused: function (r) {
            t = n.PAUSED, r && (a ? a.sessionresult = r : a = {sessionresult: r}), o(e.PAUSE, a), a = null
        }, resume: function (a) {
            t === n.NOT_PAUSED && r("Requesting resume, but the game is not paused"), t = n.NOT_PAUSED, o(e.RESUME, a)
        }, updateBalance: function () {
            o(e.UPDATE_BALANCE)
        }, setSounds: function (n) {
            o(e.SET_SOUNDS, n)
        }, getSoundState: function () {
            o(e.GET_SOUND_STATE)
        }
    };

    function s(e) {
        var n = this;
        this.vars = {}, this.length = 0, decodeURIComponent(e).replace(/[?&]+([^=&]+)=([^&]*)/gi, function (e, t, a) {
            n.vars[t] = a, n.length++
        })
    }

    s.prototype = {
        isEmpty: function () {
            return 0 === this.length
        }, get: function (e) {
            return this.vars[e] ? this.vars[e] : null
        }, has: function (e) {
            return !!this.vars[e]
        }, append: function (e, n) {
            void 0 === this.vars[e] && this.length++, this.vars[e] = n
        }
    };
    var d = {
        getEventDataPayload: function (e) {
            return e && e.payload ? "string" == typeof e.payload ? JSON.parse(e.payload) : e.payload : null
        }, UrlParams: s
    };
    var u = void 0, c = !1, l = [],
        g = ["gameLoaded", "gameLoadedHandler", "gamePausedHandler", "playTimeHandler", "handshakeSuccess", "handshakeCompleted", "handshakeFail", "handshakeFailed"];

    function f(e) {
        l.push(e)
    }

    var p = {
        init: function (e) {
            var n = new d.UrlParams(document.location.href);

            function t(e, t) {
                n.get("showrgdebuglog") && console.log("%c" + e, t)
            }

            if ("true" !== n.get("rcenable")) {
                if (!(e && e.rciframeurl && n.get("rcinterval"))) return void t("Game handles reality check", "background: #ff00f0; color: #ffffff");
                t("RgAPI handles RC (standalone mode)", "background: #222; color: #bada55"), c = !0, n.append("rciframeurl", e.rciframeurl)
            } else t("RgAPI handles RC (operator mode)", "background: #222; color: #bada55");
            if ((u = function (e) {
                if (!e.has("rciframeurl")) return {error: "Error creating RC:rciframeurl is not defined"};
                var n = e.get("rciframeurl");
                if (!n || "" === n) return {error: "RC iFrame URL defined but invalid: " + n + "}"};
                new d.UrlParams(n).isEmpty() ? n += "?" : n += "&", n += "rchistoryurl=" + e.get("rchistoryurl") + "&homeurl=" + e.get("homeurl");
                var t = document.getElementById("rg_rc_iframe");
                if (!t) {
                    (t = document.createElement("iframe")).src = n, t.id = "rg_rc_iframe", t.style.display = "none", t.style.position = "fixed", t.style.left = 0, t.style.top = 0, t.style.width = "100%", t.style.height = "100%", t.style.zIndex = 1001, t.frameBorder = 0;
                    var a = document.getElementById("rc_iframe_wrapper");
                    a ? a.appendChild(t) : document.body.appendChild(t)
                }
                return t
            }(n)).error) f(u.error); else {
                var a = function (e) {
                    if (!e.has("rcinterval")) return !1;
                    var n = Number(e.get("rcinterval"));
                    if (isNaN(n)) return {error: "Error creating RC interval: Invalid rcinterval parameter " + n};
                    if (n <= 0) return !1;
                    var t = Date.now(), a = Number(e.get("rcelapsed")) || 0;
                    return setTimeout(function e() {
                        var r = a + Math.round((Date.now() - t) / 1e3);
                        i.requestPause({realityCheck: !0, sessionTime: r}), setTimeout(e, 1e3 * n)
                    }, 1e3 * (a ? n - a % n : n)), !0
                }(n);
                a.error && f(a.error), window.addEventListener(i.Event.PAUSE, function (e) {
                    var n = e.data;
                    n && n.realityCheck && function (e) {
                        u && (c && e && u.contentWindow && u.contentWindow.postMessage({
                            rgMessage: "gprg_RealityCheckData",
                            payload: e
                        }, "*"), u.style.display = "block")
                    }(n)
                }), window.addEventListener(i.Event.RESUME, function (e) {
                    var n = e.data;
                    n && n.realityCheck && u && (u.style.display = "none")
                })
            }
        }, handlePostMessage: function (e) {
            !1 !== function (e) {
                return !!e && (!(!e.rgMessage || 0 !== e.rgMessage.indexOf("gprg_")) || !!(e.method && g.indexOf(e.method) >= 0))
            }(e) && u && u.contentWindow && u.contentWindow.postMessage(e, "*")
        }, getErrors: function () {
            return l
        }
    }, m = !1, E = null, v = !1, y = !1;
    var h = {
        operatorLoaded: function () {
            m && S()
        }, confirmHandshake: function (e) {
            y || (y = !0, window.addEventListener(i.Event.PAUSE_REQUESTED, function (e) {
                var n = e.data;
                n && n.sessionTime && _("playTimeHandler", {delay: n.sessionTime})
            }), window.addEventListener(i.Event.PAUSE, function (e) {
                var n = e.data;
                n.returnData ? _(n.returnData.method, n.returnData.params) : _("gamePausedHandler")
            })), _(e.success)
        }, pauseGame: function (e) {
            var n = {realityCheck: !0};
            e.callback && (n.returnData = {
                method: e.callback, params: function (e) {
                    if (!E) return {};
                    var n = {};
                    e.return_data && e.return_data.forEach(function (e) {
                        n[e] = E[e]
                    });
                    return JSON.stringify(n)
                }(e)
            }), i.requestPause(n)
        }, resumeGame: function () {
            i.resume({realityCheck: !0})
        }, navigateTo: function (e) {
            e && e.url && (window.location.href = e.url)
        }
    };

    function _(e, n) {
        if (v) {
            var t = {method: e, params: n};
            window.parent.postMessage(t, "*"), p.handlePostMessage(t)
        }
    }

    function S() {
        m = !0, _("gameLoadedHandler")
    }

    var P = {
        init: function (e) {
            "true" === new d.UrlParams(document.location.href).get("rcenable") && (v = !0, e && (E = e.sessionData))
        }, reportGameReady: S, handlePostMessage: function (e) {
            if (v && e && e.method) {
                var n = h[e.method], t = void 0;
                e.params && (t = e.params), "function" == typeof n && n(t)
            }
        }, disable: function () {
            v = !1
        }
    }, w = "1.4.1", R = "*", b = !1, U = !1;

    function A(e, n) {
        if (b && e) {
            var t = {rgMessage: e, payload: n};
            window.parent.postMessage(t, R), p.handlePostMessage(t)
        }
    }

    function D(e) {
        A("gprg_Warning", {message: e})
    }

    var k = {
        init: function () {
            b = !0, A("gprg_Listening")
        }, reportGameReady: function () {
            A("gprg_GameReady")
        }, userAction: function (e) {
            A("gprg_UserAction", {action: e})
        }, gameRoundStarted: function (e) {
            A("gprg_GameRoundStart", e)
        }, gameRoundEnded: function (e) {
            A("gprg_GameRoundEnd", e)
        }, reportSettings: function (e) {
            A("gprg_Settings", e)
        }, handlePostMessage: function (e) {
            if (b && e && e.rgMessage && -1 !== e.rgMessage.indexOf("oprg_")) {
                var n = d.getEventDataPayload(e);
                switch (e.rgMessage) {
                    case"oprg_Ready":
                        U || (U = !0, window.addEventListener(i.Event.PAUSE_REQUESTED, function (e) {
                            var n = e.data;
                            n && n.sessionTime && A("gprg_RealityCheckData", {sessionTime: n.sessionTime})
                        }), window.addEventListener(i.Event.PAUSE, function (e) {
                            var n = e.data;
                            A("gprg_GamePaused", n)
                        }), window.addEventListener(i.Event.RESUME, function (e) {
                            var n = e.data;
                            A("gprg_GameResumed", n)
                        }), window.addEventListener(i.Event.WARN, function (e) {
                            var n = e.data;
                            n.message && D(n.message)
                        })), P.disable(), A("gprg_Ready");
                        break;
                    case"oprg_GamePause":
                        i.requestPause(n);
                        break;
                    case"oprg_GameResume":
                        i.resume(n);
                        break;
                    case"oprg_NavigateGame":
                        n && n.url ? window.location.href = n.url : D("Missing payload.url");
                        break;
                    case"oprg_DebugSettings":
                        A("gprg_DebugSettings", {id: "RgPostMessageAPI v" + w});
                        break;
                    case"oprg_DebugErrors":
                        A("gprg_DebugErrors", p.getErrors());
                        break;
                    case"oprg_Ping":
                        n && n.id ? A("gprg_Pong", {id: n.id}) : A("gprg_Pong");
                        break;
                    case"oprg_UpdateBalance":
                        i.updateBalance();
                        break;
                    case"oprg_SetSounds":
                        i.setSounds(n);
                        break;
                    case"oprg_GetSoundState":
                        i.getSoundState();
                        break;
                    default:
                        D("Unknown rgMessage " + e.rgMessage)
                }
            }
        }
    };

    function M(e) {
        var n = e.data;
        n && (k.handlePostMessage(n), P.handlePostMessage(n), p.handlePostMessage(n))
    }

    return {
        init: function (e, n, t) {
            k.init(), P.init(n), function () {
                var e = document.getElementById("rg_pause_modal");
                if (!e) {
                    (e = document.createElement("div")).id = "rg_pause_modal", e.style.position = "fixed", e.style.left = 0, e.style.top = 0, e.style.width = "100%", e.style.height = "100%", e.style.backgroundColor = "rgba(0, 0, 0, 0.5)", e.style.display = "none", e.style.zIndex = 1e3;
                    var n = document.getElementById("rg_rc_iframe");
                    if (n) n.parent.insertBefore(e, n); else {
                        var t = document.getElementById("rc_iframe_wrapper");
                        t ? t.appendChild(e) : document.body.appendChild(e)
                    }
                }
                window.addEventListener(i.Event.PAUSE, function (n) {
                    var t = n.data;
                    t && t.autoPlayOnly || (e.style.display = "block")
                }), window.addEventListener(i.Event.RESUME, function (n) {
                    var t = n.data;
                    t && t.autoPlayOnly || (e.style.display = "none")
                })
            }(), e && p.init(t), window.addEventListener("message", M)
        }, reportGameReady: function () {
            k.reportGameReady(), P.reportGameReady()
        }, reportPaused: function (e) {
            i.paused(e)
        }, userAction: function (e) {
            if (0 !== e.indexOf("action:")) window.location = e; else {
                var n = e.slice("action:".length);
                k.userAction(n)
            }
        }, gameRoundStarted: k.gameRoundStarted, gameRoundEnded: k.gameRoundEnded, reportSettings: k.reportSettings
    }
});
