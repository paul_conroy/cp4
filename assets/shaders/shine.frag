precision mediump float;

varying vec2 vTextureCoord;
uniform sampler2D uSampler;

uniform float position;
uniform float intensity;

void main(void) 
{	
	vec4 texSample = texture2D(uSampler, vTextureCoord);

	float tmp = vTextureCoord.x + vTextureCoord.y - position;	
	
	tmp = intensity * exp (tmp * tmp / -0.01) * texSample.w;
	
	vec4 scaledComp = texSample * tmp;	
	texSample = texSample + scaledComp;
	
	gl_FragColor = texSample;
}