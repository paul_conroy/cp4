precision mediump float;

varying vec2 vTextureCoord;
uniform sampler2D uSampler;

uniform float intensity;

void main(void) 
{	
	vec4 col = texture2D(uSampler, vTextureCoord);	
	
	float desatVal = (0.3*col.r + 0.59*col.g + 0.11*col.b) / 1.25;
	
	vec4 desat = vec4(desatVal * 0.6 , desatVal * 0.82, desatVal, col.w * 0.8);	
	
	vec4 final = col * (1.0 - intensity) + desat * intensity;
	
	gl_FragColor = final;
}